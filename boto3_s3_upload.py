import boto3
import os

#bucket = "s3://gfl/Graph/"
credentials = {
    #in the s3cmd config for your project
    "access_key": 'yourkey',
    "secret_key": 'yoursecretkey',
    "endpoint": 'https://s3...',
    "bucket": 'yourbucket name'
}

# create S3 session
session = boto3.client(
	's3',
	aws_access_key_id=credentials["access_key"],
	aws_secret_access_key=credentials["secret_key"],
	endpoint_url=credentials["endpoint"])

response = session.list_buckets()
paginator = session.get_paginator('list_objects_v2')
pages = paginator.paginate(Bucket='gfl', Prefix='Graph')

count = 0
for page in pages:
    for obj in page['Contents']:
        print(obj['Key'])
        count += 1
print(count)


# again assumes boto.cfg setup, assume AWS S3
count = 0
for key in session.list_objects(Bucket='gfl')['Contents']:
    print(key)
    count += 1

print(count)
# upload files from specified dir
upload_dir = "/home/your/dir"

# read dir
file_list = os.scandir(upload_dir)

for local_file_name in file_list:
    file_path = os.path.abspath(local_file_name)
    print(file_path)
    session.upload_file(str(file_path), credentials['bucket'], str(local_file_name))
    