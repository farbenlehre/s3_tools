'''
#script for rename gfl buckets
jerome

'''
import sys
import subprocess

gfl_cmd_config = "s3cmd -c ../.s3cfg_gfl"
bucket = "s3://gfl/Graph/"
complement = "SBB_SPK/"
src_dir = bucket + complement

ls_cmd = gfl_cmd_config + " ls -r " + src_dir
all_ls_volumes = subprocess.check_output(ls_cmd, shell=True)
out = all_ls_volumes.splitlines()
string_set = [ str(x) for x in out]
volumes_set = [x.split(src_dir) for x in string_set]
volumes_set = [x[1] for x in volumes_set]
bucket_name_dict  = [x.split("/") for x in volumes_set]

for bucket, name in bucket_name_dict:
    if not name.startswith("SBB_SPK_") and not name.startswith("0000"):
        path_to_del_file = src_dir + bucket + "/" + name.replace("\'", "")
        cp_cmd = ("%s rm %s" % (gfl_cmd_config, path_to_del_file))
        #print(cp_cmd)
        subprocess.call(cp_cmd, shell=True)

