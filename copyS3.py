#!/usr/bin/env python
'''
#script for rename gfl buckets
jerome

'''
import sys
import subprocess

gfl_cmd_config = "s3cmd -c ../.s3cfg_gfl"
bucket = "s3://gfl/Graph/"
complement = "SBB_SPK/"
src_dir = bucket + complement

ls_cmd = gfl_cmd_config + " ls -r " + src_dir
all_ls_volumes = subprocess.check_output(ls_cmd, shell=True)
out = all_ls_volumes.splitlines()

count = 0
for o in all_ls_volumes:
    print(type(o))
    count += 1

print("they are " + str(count))


volumes_set = [x.strip() for x in out]
volumes_set = [x.replace(src_dir, "") for x in volumes_set]

for name in volumes_set:
    recursive_ls_cmd = ls_cmd + name
    all_ls_files = subprocess.check_output(recursive_ls_cmd, shell=True)
    out = all_ls_files.splitlines()
    files_names_set = [x.split(src_dir) for x in out]
    files_names_set = [x[1].replace(name, "") for x in files_names_set]
    print(name)
    if complement not in name:
        name_new = complement + "_" + name

        for ls_file in files_names_set:

            old_s3 = src_dir + name + ls_file
            new_s3 = src_dir +"SBB_SPK_Slg_Darmst_F_1e_1821_Seebeck" + ls_file
            cp_cmd = ("%s mv %s %s" % (gfl_cmd_config, old_s3, new_s3))
            #subprocess.call(cp_cmd, shell=True)
            print(cp_cmd)
            #rm_cmd = ("%s rm %s" % (gfl_cmd_config, old_s3))
            #subprocess.call(rm_cmd, shell=True)
            #print(rm_cmd)