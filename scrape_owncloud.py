import owncloud
import sys
import subprocess
import boto3
from PIL import Image

# owncloud part
print("start with scrape owncloud..")

oc = owncloud.Client('https://owncloud.gwdg.de')
oc.login('yourname@sub.uni-goettingen.de', 'password')

path= "Goethes Farbenlehre/Graph_Archiv/"
list_img_owncloud = []
list_owncloud_fileobjects =  []

def list_of_img_owncloud(path):
    for file_or_dir in oc.list(path):
        if file_or_dir.is_dir():
           list_of_img_owncloud(file_or_dir.get_path())
        else:
            if file_or_dir.get_path() +"/"+ file_or_dir.get_name() not in list_img_owncloud and not ".pdf" in file_or_dir.get_name() :
                list_img_owncloud.append(file_or_dir.get_path() +"/"+ file_or_dir.get_name())
                list_owncloud_fileobjects.append(file_or_dir)

list_of_img_owncloud(path)

print("Images in Owncloud: "+ str(len(list_img_owncloud)))

cutted_list_img_owncloud = [x.replace("/Goethes Farbenlehre/Graph_Archiv/", "") for x in list_img_owncloud]
cutted_list_img_owncloud = [x.replace(".tif", "") for x in cutted_list_img_owncloud]
cutted_list_img_owncloud = [x.replace(".TIF", "") for x in cutted_list_img_owncloud]

# create S3 session
print("start s3 part")

#bucket = "s3://gfl/Graph/"
credentials = {
    #in the s3cmd config for your project
    "access_key": 'yourkey',
    "secret_key": 'yoursecretkey',
    "endpoint": 'https://s3...',
    "bucket": 'yourbucket name'
}

session = boto3.client(
	's3',
	aws_access_key_id=credentials["access_key"],
	aws_secret_access_key=credentials["secret_key"],
	endpoint_url=credentials["endpoint"])

paginator = session.get_paginator('list_objects_v2')
pages = paginator.paginate(Bucket='gfl', Prefix='Graph')

#add keys to list for trim str
count = 0
s3_list_keys = []
for page in pages:
    for obj in page['Contents']:
        s3_list_keys.append(obj['Key'])
        count += 1

print("Images in s3: "+  str(count))

img_s3_list = [x.replace("Graph/", "").replace(".jpg", "") for x in s3_list_keys]

#retuns images name not in s3
def match_lists(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    return set2.difference(set1)

imgs_diff = match_lists(cutted_list_img_owncloud,img_s3_list)
print("there are " + str(len(imgs_diff)) + " different imgs in owncloud and s3" )

imgs_not_in_s3 = []
count_not_s3 = 0
for img in cutted_list_img_owncloud:
    if img not in img_s3_list:
        imgs_not_in_s3.append(img)
        count_not_s3 += 1

print("there are "+str(count_not_s3)+" imgs missing in s3")
if count_not_s3 > 0:
    print("these imgs are not in s3:")
[print(" - "+i) for i in imgs_not_in_s3 ]

for img in imgs_not_in_s3:
    try:
        tiff_path = "Goethes Farbenlehre/Graph_Archiv/" +img +".tif" 
        tiff_file = oc.get_file(tiff_path)
        path_split_list = img.split("/")
        name_tiff = path_split_list[-1]+".tif"
        im = Image.open(name_tiff)
        im.save(path_split_list[-1]+".jpg")
        s3_path_jpg = "Graph/" +img +".jpg"
        session.upload_file(path_split_list[-1]+".jpg", credentials['bucket'], s3_path_jpg)
        print("img :"+s3_path_jpg+" succesfully load to s3" )
    except:
        try:
            
            tiff_path = "Goethes Farbenlehre/Graph_Archiv/" +img +".TIF" 
            tiff_file = oc.get_file(tiff_path)
            path_split_list = img.split("/")
            name_tiff = path_split_list[-1]+".TIF"
            im = Image.open(name_tiff)
            im.save(path_split_list[-1]+".jpg")
            s3_path_jpg = "Graph/" +img +".jpg"
            session.upload_file(path_split_list[-1]+".jpg", credentials['bucket'], s3_path_jpg)
            print("img :"+s3_path_jpg+" succesfully load to s3" )
        except Exception as e:
            print("Error at loading tif/TIF from ownccloud: " +str(e))

print("...the end")
